class TodoSchema
  include Swagger::Blocks
  swagger_schema :Todo do
    key :required, [:name, :description]
    property :name do
      key :type, :string
    end
    property :description do
      key :type, :string
    end
  end

  swagger_schema :CreateTodoResponse do
    key :required, [:id]
    property :id, type: :object do
      key :oid, :int
    end
    key :'$ref', :Todo
  end
end