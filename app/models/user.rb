class User
  include BCrypt
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActiveModel::SecurePassword

  has_many :todos

  field :name, type: String
  field :email, type: String
  field :password , type: String

  validates_presence_of :name, :email, :password
  validates :password,  length: { minimum: 8, maximum: 16 }
  validates :email, uniqueness: true

  before_save :encrypt_password

  def encrypt_password
    self.password = Password.create(self.password)
  end
  

  def self.authenticate(params)
    begin
      user = self.find_by(email: params[:email])
    rescue
      return nil
    end
    if Password.new(user.password) == params[:password]
      return user
    else
      return nil
    end
  end
end
