# frozen_string_literal: true

class ApplicationController < ActionController::API
    def authorize_request
        header = request.headers['Authorization']
        header = header.split(' ').last if header
        begin
            @decoded = JsonWebToken.decode(header)
            return @decoded
        rescue JWT::DecodeError => e
            render json: { errors: e.message}, status: :unauthorized
        end
    end
end
