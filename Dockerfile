FROM ruby:2.6.3

WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . ./

CMD ["rails" , "s", "-b", "0.0.0.0"]
EXPOSE 3000