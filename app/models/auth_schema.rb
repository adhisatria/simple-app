class AuthSchema
  include Swagger::Blocks

  swagger_schema :LoginInfo do
    key :required, [:token]
    property :token do
      key :type, :string
    end
  end

  swagger_schema :LoginInput do
    key :required, [:email, :password]
    property :email do
      key :type, :string
    end
    property :password do
      key :type, :string
    end
  end

  swagger_schema :RegisterInput do
    key :required, [:name, :email, :password]
    property :name do
      key :type, :string
    end

    property :email do
      key :type, :string
    end

    property :password do
      key :type, :string
    end
  end

  swagger_schema :User do
    key :required, [:id, :name]
    property :id do
      key :type, :integer
      key :format, :int64
    end
    property :name do
      key :type, :string
    end
    property :tag do
      key :type, :string
    end
  end

end