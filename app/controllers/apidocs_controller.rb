class ApidocsController < ActionController::Base
  include Swagger::Blocks
  
  swagger_root do
    key :swagger, '2.0'
    info do
      key :version, '1.0.0'
      key :title, 'Swagger Simple Project'
      key :description, 'A sample API with authentication and todo list feature'
      contact do
        key :name, 'adhi'
      end
    end

    security_definition :jwt_auth do
      key :type, :apiKey
      key :name, :Authorization
      key :in, :header
    end

    key :schemes, ['http']
    key :host, 'localhost:3000'
    key :basePath, '/'
    key :consumes, ['application/json']
    key :produces, ['application/json']
  end

    # A list of all classes that have swagger_* declarations.
    SWAGGERED_CLASSES = [
      UserSchemaController,
      TodoSchemaController,
      ErrorModel,
      AuthSchema,
      TodoSchema,
      self,
    ].freeze
  
  
    def index
      render json: Swagger::Blocks.build_root_json(SWAGGERED_CLASSES)
    end

end