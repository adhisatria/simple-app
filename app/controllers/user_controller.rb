# frozen_string_literal: true

class UserController < ApplicationController
  include Import['user_service']

  def register
    meta, data, errors = user_service.register(register_params)
    if errors 
      return render json: {meta: meta, data: data, errors: errors}, status: 400
    end
    render json: {meta: meta, data: UserSerializer.new(data), errors: []}
  end

  def login
    meta, data, errors = user_service.login(login_params)
    if errors
      return render json: {meta: meta, data: data, errors: errors}, status: 400
    end
    render json: {meta: meta, data: data, errors: []}
  end

  private

  def register_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  def login_params
    params.require(:user).permit(:email, :password)
  end

end
