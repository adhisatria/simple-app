# frozen_string_literal: true
# require 'sidekiq/web'

Rails.application.routes.draw do
  get '/api' => redirect('/swagger-ui/dist/index.html?url=http://localhost:3000/apidocs')
  resources :apidocs, only: [:index] 
  post '/register', to: 'user#register'
  post '/login', to: 'user#login'
  post '/todo', to: 'todo#create'
  get '/todo', to: 'todo#index'
  get '/todo/:id', to: 'todo#show'
  put '/todo/:id', to: 'todo#update'
  delete '/todo/:id', to: 'todo#destroy'
  # mount Sidekiq::Web => '/sidekiq'
end
