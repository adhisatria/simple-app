class TodoSchemaController < ActionController::Base
  include Swagger::Blocks

  swagger_path '/todo' do
    operation :post do
      key :summary, 'Create todo list'
      key :description, 'Add new to do for current user'
      key :operationId, 'createTodo'
      key :tags, [
        'todo'
      ]
      security do
        key :jwt_auth, []
      end
      parameter do
        key :name, :body
        key :in, :body
        key :description, 'Create todo input'
        schema do
          key :'$ref', :Todo
        end
      end

      response 200 do
        key :description, 'success todo response'
        schema do
          property :meta, type: :object
          property :data, type: :object do
            key :'$ref', :CreateTodoResponse
          end
          property :errors, type: :object
        end
      end

      response 400 do 
        key :description, 'bad request when create todo response'
        schema do
          property :meta, type: :object
          property :data, type: :object
          property :errors, type: :object do
            key :'$ref', :ErrorModel 
          end
        end
      end
    end

    operation :get do
      key :summary, 'Get todo list current user'
      key :description, 'Get all user todo list'
      key :operationId, 'getUserTodo'
      key :tags, [
        'todo'
      ]
      security do
        key :jwt_auth, []
      end
      response 200 do
        key :description, 'todo list user'
        schema do
          property :meta, type: :object
          property :data, type: :object do
            key :type, :array
            items do
              key :'$ref', :CreateTodoResponse              
            end
          end
          property :errors, type: :object
        end
      end

      response 400 do 
        key :description, 'bad request when get user todo'
        schema do
          property :meta, type: :object
          property :data, type: :object
          property :errors, type: :object do
            key :'$ref', :ErrorModel 
          end
        end
      end
    end
  end

  swagger_path '/todo/{id}' do
    parameter do
      key :name, :id
      key :in, :path
      key :description, 'ID of todo'
      key :required, true
      key :type, :string
    end
    operation :get do
      key :summary, 'Get todo'
      key :description, 'Get todo by id'
      key :operationId, 'getTodo'
      key :tags, [
        'todo'
      ]
      security do
        key :jwt_auth, []
      end
      response 200 do
        key :description, 'success get a todo data'
        schema do
          property :meta, type: :object
          property :data, type: :object do
            key :'$ref', :CreateTodoResponse
          end              
          property :errors, type: :object
        end
      end

      response 400 do 
        key :description, 'bad request when get todo'
        schema do
          property :meta, type: :object
          property :data, type: :object
          property :errors, type: :object do
            key :'$ref', :ErrorModel 
          end
        end
      end
    end
    operation :delete do
      key :summary, 'Delete current todo'
      key :description, 'delete todo data'
      key :operationId, 'deleteTodo'
      key :produces, [
        'application/json'
      ]
      key :tags, [
        'todo'
      ]
      security do
        key :jwt_auth, []
      end
      response 204 do
        key :description, 'success get a todo data'
        schema do
          property :meta, type: :object do
            key :message, type: :string
          end
          property :data, type: :object
          property :errors, type: :object
        end
      end

      response 400 do 
        key :description, 'bad request when delete todo'
        schema do
          property :meta, type: :object
          property :data, type: :object
          property :errors, type: :object do
            key :'$ref', :ErrorModel 
          end
        end
      end
    end
    operation :put do
      key :summary, 'Update current todo'
      key :description, 'Update todo data'
      key :operationId, 'updateTodo'
      key :produces, [
        'application/json'
      ]
      key :tags, [
        'todo'
      ]
      security do
        key :jwt_auth, []
      end
      parameter do
        key :name, :body
        key :in, :body
        key :description, 'Update todo input'
        schema do
          key :'$ref', :Todo
        end
      end
      response 200 do
        key :description, 'success get a todo data'
        schema do
          property :meta, type: :object
          property :data, type: :object do
            key :'$ref', :CreateTodoResponse              
          end
          property :errors, type: :object
        end
      end

      response 400 do 
        key :description, 'bad request when get todo'
        schema do
          property :meta, type: :object
          property :data, type: :object
          property :errors, type: :object do
            key :'$ref', :ErrorModel 
          end
        end
      end
    end
  end

  
end