class UserSchemaController < ActionController::Base
  include Swagger::Blocks

  swagger_path '/login' do
    operation :post do
      key :summary, 'User login'
      key :description, 'Returns a token of current user'
      key :operationId, 'login'
      key :tags, [
        'authentication'
      ]
      parameter do
        key :name, :body
        key :in, :body
        key :description, 'Login input'
        key :required, true
        schema do
          key :'$ref', :LoginInput
        end
      end
      response 200 do
        key :description, 'login response'
        schema do
          property :meta, type: :object
          property :errors, type: :object
          property :data, type: :object do
            key :'$ref', :LoginInfo
          end
        end
      end
      response 400 do
        key :description, 'bad request'
        schema do
          property :meta, type: :object
          property :errors, type: :object do
            key :'$ref', :ErrorModel
          end
          property :data, type: :object
        end
      end
    end
  end

  swagger_path '/register' do
    operation :post do
      key :summary, 'Register new user'
      key :description, 'Return user info'
      key :operationId, 'register'
      key :tags, [
        'authentication'
      ]
      parameter do
        key :name, :body
        key :in, :body
        key :description, 'Register input'
        key :required, true
        schema do
          key :'$ref', :RegisterInput
        end
      end
      response 200 do
        key :description, 'register response'
        schema do
          property :meta, type: :object
          property :errors, type: :object
          property :data, type: :object do
            key :'$ref', :LoginInfo
          end
        end
      end
      response 400 do
        key :description, 'bad request'
        schema do
          property :meta, type: :object
          property :errors, type: :object do
            key :'$ref', :ErrorModel
          end
          property :data, type: :object
        end
      end
    end
  end

  
end