# frozen_string_literal: true

class MyContainer
  extend Dry::Container::Mixin

  register 'user_service' do
    UserService.new
  end

  register 'todo_service' do
    TodoService.new
  end
end

Import = Dry::AutoInject(MyContainer)
