
class UserService
  include BCrypt

  def register(params)    
    @user = User.new(params)
    if @user.valid?
      @user.save
      return nil, @user, nil
    else
      return nil, nil, @user.errors
    end
  end

  def login(params)
    user = User.authenticate(params) 
    if !user.nil?
      payload = {
        email: user.email,
        user_id: user.id
      }
      token = ::JsonWebToken.encode(payload)
      return nil, token, nil
    else
      return nil, nil, 'User not found'
    end
  end

end
