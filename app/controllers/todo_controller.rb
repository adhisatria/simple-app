class TodoController < ApplicationController
    include Import['todo_service']
    before_action :authorize_request

    def index
        meta, data, errors = todo_service.index(@decoded[:user_id])
        if errors
            return render json: {meta: meta, data: data, errors: errors}, status: 400
        end
        render json: {meta: meta, data: ActiveModel::Serializer::ArraySerializer.new(data, each_serializer: TodoSerializer), errors: errors}
        
    end

    def create
        meta, data, errors = todo_service.create(todo_params, @decoded[:user_id])
        if errors
            return render json: {meta: meta, data: TodoSerializer.new(data), errors: errors}, status: 400
        end
        render json: {meta: meta, data: TodoSerializer.new(data), errors: errors}
    end

    def show
        meta, data, errors = todo_service.find_by(params[:id])
        if errors
            return render json: {meta: meta, data: TodoSerializer.new(data), errors: errors}, status: 400
        end
        render json: {meta: meta, data: TodoSerializer.new(data), errors: errors}    end

    def update
        meta, data, errors = todo_service.update(params[:id], todo_params)
        if errors
            return render json: {meta: meta, data: TodoSerializer.new(data), errors: errors}, status: 400
        end
        render json: {meta: meta, data: TodoSerializer.new(data), errors: errors}    end

    def destroy
        meta, data, errors = todo_service.destroy(params[:id])
        if errors
            return render json: {meta: meta, data:data, errors: errors}, status: 400
        end
        render json: {meta: meta, data:data, errors: errors}
    end

    
    private

        def todo_params
            params.require(:todo).permit(:name, :description)
        end
end