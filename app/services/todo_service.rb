class TodoService

    def index(user_id)
        @todos = Todo.where(user_id: BSON::ObjectId.from_string(user_id['$oid']))
        return nil, @todos, nil
    end

    def create(params, user_id)
        @todo = Todo.new(params)
        @todo.user_id = user_id
        if @todo.save
            return nil, @todo, nil
        else
            return nil, nil, @todo.errors
        end
    end

    def find_by(id)
        @todo = Todo.find(id)
        return nil, @todo, nil
    end

    def update(id, params)
        @todo = Todo.find(id)
        if @todo.update(params)
            return nil, @todo, nil
        else
            render nil, nil, @todo.errors
        end
    end

    def destroy(id)
        @todo = Todo.find(id)
        if @todo.destroy
            return {message: "Success delete todo"}, nil, nil
        else
            return {message: "Fail delete todo"}, nil, @todo.errors
        end
    end

end