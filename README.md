# README

## Todo App

Simple project

## Prerequisite

- docker
- docker-compose

## How to run

- Copy .env.example to .env
- Setup your .env
- Then you can run docker-compose in this repos through the following command

```
docker-compose up -d --build
```
